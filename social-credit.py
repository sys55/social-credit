from __future__ import print_function
import cv2 as cv
import argparse
import time
import numpy as np
import os

class Face:
    def __init__(self, x, y, w, h, img, modules):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

        self.img = img
        self.roi = img[y : y + h, x : x + w]

        self.modules = modules

        try:
            self.eyes = self.find_eyes()
            self.age = self.calc_age()
            self.gender = "Male" if self.get_gender() else "Female"
            self.happiness = self.calc_happiness()
        except Exception as e:
            self.age  = "Dead"
            self.gender = "No sexism"
            self.happiness = "Depression"

    def calc_age(self):
        AGE_BUCKETS = ["(0-2)", "(4-6)", "(8-12)", "(15-20)", "(25-32)", "(38-43)", "(48-53)", "(60-100)"]

        faceBlob = cv.dnn.blobFromImage(self.roi, 1.0, (227, 227), (78.4263377603, 87.7689143744, 114.895847746), swapRB=False) 
        self.modules.age.setInput(faceBlob)
        preds = self.modules.age.forward()
        i = preds[0].argmax()
        age = AGE_BUCKETS[i]

        return age

    def get_gender(self):
        GENDER_LIST = [1, 0]

        gender_net = self.modules.gender_module
        blob = cv.dnn.blobFromImage(self.roi, scalefactor=1.0, size=(227, 227), mean=(78.4263377603, 87.7689143744, 114.895847746), swapRB=False, crop=False)
        # Predict Gender
        gender_net.setInput(blob)
        gender_preds = gender_net.forward()
        i = gender_preds[0].argmax()
        gender = GENDER_LIST[i]
        return gender # 0 : male, 1 : female

    def calc_happiness(self):
        EMOTIONS = ['Neutral', 'Happy', 'Surprise', 'Sad', 'Anger', 'Disgust', 'Fear', 'Contempt']

        gray = cv.cvtColor(self.roi, cv.COLOR_BGR2GRAY)
        resized = cv.resize(gray, (64, 64))
        processed = resized.reshape(1, 1, 64, 64)

        self.modules.emotion.setInput(processed)
        output = self.modules.emotion.forward()

        expanded = np.exp(output - np.max(output))
        probs = expanded / expanded.sum()

        prob = np.squeeze(probs)

        return EMOTIONS[prob.argmax()]

    def find_eyes(self):
        eyes = self.modules.eye.detectMultiScale(self.roi)
        return eyes

    def get_corner(self): # which side the face is at in the image. data box will be drawn accordingly.
        w, h, _ = self.img.shape

        midx = w // 2
        midy = h // 2

        # 2   1
        # 3   4

        if self.x <= midx:
            if self.y <= midy:
                return 2
            else:
                return 3

        else:
            if self.y <= midy:
                return 1
            else:
                return 4


    def get_dat_rect(self):
        corner = self.get_corner()
        
        radius = int(round((self.w + self.h)*0.25))
        center = (self.x + self.w // 2, self.y + self.h // 2)

        RADIUSX_COR = 50

        if corner == 1:
            x = center[0] - radius + RADIUSX_COR
            y = center[1] + radius
        elif corner == 2:
            x = center[0] + radius - RADIUSX_COR
            y = center[1] + radius
        elif corner == 3:
            x = center[0] + radius - RADIUSX_COR
            y = center[1] - radius
        else:
            x = center[0] - radius + RADIUSX_COR
            y = center[1] - radius
            
        w = 100
        h = 60

        return x, y, w, h

    def draw(self) -> np.array:

        x, y = self.x, self.y
        w, h = self.w, self.h
        img = self.img

        # face
        cv.rectangle(img, (x, y), (x + w, y + h), (255, 255, 0), 2)

        # # eyes
        # data = []
        # for (x2, y2, w2, h2) in self.eyes:
        #     eye_center = (x + x2 + w2//2, y + y2 + h2//2)
        #     radius = int(round((w2 + h2)*0.25))
        #     data.append(eye_center)
        #     data.append(radius)
        #     img = cv.circle(img, eye_center, radius, (255, 0, 0 ), 4)

        
        # data
        rectx, recty, rectw, recth = self.get_dat_rect()
        img1 = img.copy()
        cv.rectangle(img1, (rectx, recty), (rectx + rectw, recty + recth), (0, 0, 0), -1)
        font = cv.FONT_HERSHEY_PLAIN
        cv.putText(img1, "G: " + str(self.gender), (rectx, recty + 15), font, 1, (255, 255, 255), 1, cv.LINE_AA)
        cv.putText(img1, "A: " + self.age, (rectx, recty + 35), font, 1, (255, 255, 255), 1, cv.LINE_AA)
        if type(self.happiness) == int:
            cv.putText(img1, "E: %" + str(self.happiness), (rectx, recty + 55), font, 1, (255, 255, 255), 1, cv.LINE_AA)
        else:
            cv.putText(img1, "E: " + str(self.happiness), (rectx, recty + 55), font, 1, (255, 255, 255), 1, cv.LINE_AA)

        alpha = .5
        cv.addWeighted(img1, alpha, img, 1 - alpha, 0, img)

        return img


class Modules:
    def __init__(self, face, eye, emotion, age, gender):
        self.eye = eye
        self.emotion = emotion

        self.age = age
        self.face = face
        self.gender_module = gender


def draw_fps(frame, t2):
    t1 = time.time()
    fps = int(1 / (t1 - t2))
    font = cv.FONT_HERSHEY_SIMPLEX
    cv.putText(frame, str(fps), (10, 30), font, 1, (100, 255, 0), 3, cv.LINE_AA)


def get_face_cords(frame, module):
    # frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    # frame_gray = cv.equalizeHist(frame_gray)

    net = module
    h, w = frame.shape[:2]
    blob = cv.dnn.blobFromImage(cv.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
    net.setInput(blob)
    detections = net.forward()
    face_cords = []
    for i in range(0, detections.shape[2]):

        confidence = detections[0, 0, i, 2]
        if confidence > 0.8:
            box = detections[0, 0, i, 3 : 7] * np.array([w, h, w, h])
            startX, startY, endX, endY = box.astype("int")
            # face_cords.append((startX - 7, startY - 7, endX - startX + 7, endY - startY + 7))
            face_cords.append((startX - 7, startY - 7, endX - startX + 7, endY - startY + 7))
    # face_cords = module.detectMultiScale(frame_gray)

    return face_cords

def draw_frame(frame, modules, t2, scan, faces):
    if scan:
        face_cords = get_face_cords(frame, modules.face)
        for (x,y,w,h) in face_cords:
            face = Face(x, y, w, h, frame, modules)
            faces.append(face)
            frame = face.draw() 
    else:
        for face in faces:
            frame = face.draw()

    draw_fps(frame, t2)

    return frame, faces


def process(frame, modules, t2, scan, faces):

    frame, faces = draw_frame(frame, modules, t2, scan, faces)

    return frame, faces

def preprocess(frame):

    frame = cv.flip(frame, 1)

    return frame


def main():

    eye_module = cv.CascadeClassifier(cv.data.haarcascades + "haarcascade_eye_tree_eyeglasses.xml")
    smile_module = cv.CascadeClassifier(cv.data.haarcascades + "haarcascade_smile.xml")

    emotion_module = cv.dnn.readNetFromONNX("emotion-ferplus-8.onnx")

    age_module = cv.dnn.readNet("age_deploy.prototxt", "age_net.caffemodel")
    face_module = cv.dnn.readNetFromCaffe("deploy.prototxt.txt", "res10_300x300_ssd_iter_140000.caffemodel")
    gender_module = cv.dnn.readNetFromCaffe("deploy_gender.prototxt", "gender_net.caffemodel")


    DET_TIME = 0.1

    modules = Modules(face_module, eye_module, emotion_module, age_module, gender_module)
    cap = cv.VideoCapture(0)
    last_det = time.time() # last detection
    faces = []
    scan = 1 # detect faces
    while True:
        ret, frame = cap.read()

        t2 = time.time() # for calc fps

        if time.time() - last_det  >= DET_TIME:
            last_det = time.time()
            scan = 1
            faces = []


        # frame = cv.imread("/home/sys55/Pictures/download.jpg")
        frame = preprocess(frame)
        frame, faces = process(frame, modules, t2, scan, faces)
        cv.imshow('Capture - Face detection', frame)
        if cv.waitKey(10) == 27:
            break

        scan = 0 


if __name__ == "__main__":
    main()
